package pt.antoniofreire.sensordata.OBDSensor;


import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.Sensor;
import android.icu.text.RelativeDateTimeFormatter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pires.obd.commands.SpeedCommand;
import com.github.pires.obd.commands.control.TroubleCodesCommand;
import com.github.pires.obd.commands.engine.RPMCommand;
import com.github.pires.obd.commands.protocol.EchoOffCommand;
import com.github.pires.obd.commands.protocol.LineFeedOffCommand;
import com.github.pires.obd.commands.protocol.ObdResetCommand;
import com.github.pires.obd.commands.protocol.ResetTroubleCodesCommand;
import com.github.pires.obd.commands.protocol.SelectProtocolCommand;
import com.github.pires.obd.enums.ObdProtocols;


import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import pt.antoniofreire.sensordata.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class OBDFragment extends Fragment {

    private View v;
    private TextView tvBtAdapterStat;
    private TextView tvObdStatus;
    private TextView tvObdEngineSpeed;
    private TextView tvObdEngineRpm;
    private BluetoothAdapter btAdapter;
    private List<String> btDevicesList;
    private List<String> btDevicesListAddress;
    private String selectedDeviceAddress;
    private Button btk_getBtList;
    private BluetoothDevice device;
    private UUID uuid;
    private BluetoothSocket socket;
    private BluetoothSocket socketFallback;
    private Boolean bollConnect = false;

    public OBDFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d("OBD","OBD Fragment created");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_obd, container, false);
        Log.d("OBD","OBD Fragment view created");

        tvBtAdapterStat = (TextView) v.findViewById(R.id.bt_stats);
        btk_getBtList = (Button) v.findViewById(R.id.btk_select_bt_adaptor);
        tvObdStatus = (TextView) v.findViewById(R.id.elm_stats);
        tvObdStatus.setText("Disconnected");
        tvObdStatus.setTextColor(Color.parseColor("#ff0000"));
        tvObdEngineSpeed = (TextView) v.findViewById(R.id.vall_engineSpeed);
        tvObdEngineRpm = (TextView) v.findViewById(R.id.vall_engineRpm);

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        // Check if the device supports Bluetooth
        if (btAdapter != null) {
            // if Bluetooth is Enabled
            if(btAdapter.isEnabled()){
                tvBtAdapterStat.setText("ON");
                tvBtAdapterStat.setTextColor(Color.parseColor("#33cc33"));
            }else{
                tvBtAdapterStat.setText("OFF");
                tvBtAdapterStat.setTextColor(Color.parseColor("#ff0000"));
                // Bluetooth activate dialog
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }

            // Dialog to show available paired devices list on btk click
            btk_getBtList.setEnabled(true);
            btk_getBtList.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
                    // If there are paired devices
                    if (pairedDevices.size() > 0) {

                        // Simple adapter for bt items list
                        btDevicesList = new ArrayList<String>();
                        btDevicesListAddress = new ArrayList<String>();

                        // Loop through paired devices
                        for (BluetoothDevice device : pairedDevices) {
                            // add bt devices to the adapter
                            btDevicesList.add(device.getName());


                            btDevicesListAddress.add(device.getAddress());
                        }

                        // Convert arrayList to a simple array for the dialog
                        String[] items = new String[btDevicesList.size()];
                        items = btDevicesList.toArray(items);

                        // build the dialog
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Choose Bluetooth Device");
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Save the selected bt device address
                                selectedDeviceAddress = btDevicesListAddress.get(i);
                                Log.d("OBD","Selected BT device: "+selectedDeviceAddress);
                                btCon();

                            }
                        });

                        // create alert dialog
                        AlertDialog alertDialog = builder.create();

                        // show it
                        alertDialog.show();

                    }else{
                        Toast.makeText(getActivity(),"No paired bluetooth devices available",Toast.LENGTH_SHORT);
                    }
                }
            });
        }
        return v;
    }

    @Override
    public void onResume() {
        // Executa quando é minimizada a aplicaç\ao e quando o fragmento é inicializado
        super.onResume();
        Log.d("OBD","OBD Fragment - onResume");
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        // Register sensor Listener's
        getActivity().registerReceiver(btStatsReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister Listener's
        getActivity().unregisterReceiver(btStatsReceiver);
        Log.d("OBD","OBD Fragment - onPause");
    }

    public void btCon(){
        Log.d("OBD","OBD Get Remote Device");
        device = btAdapter.getRemoteDevice(selectedDeviceAddress);
        uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        try {
            Log.d("OBD","OBD Open Socket");
            socket = device.createInsecureRfcommSocketToServiceRecord(uuid);
        } catch (IOException e) {
            e.printStackTrace();
            bollConnect = false;
            Toast.makeText(getActivity(),"Unable to connect to device",Toast.LENGTH_SHORT);
        }
        try {
            Log.d("OBD","OBD Socket Connect");
           // socket = BluetoothManager.connect(device);
            socket.connect();
            bollConnect = true;
            tvObdStatus.setText("Connected");
            tvObdStatus.setTextColor(Color.parseColor("#33cc33"));
            btInit();
        } catch (IOException e) {

            Log.e("OBD", "There was an error while establishing Bluetooth connection. Falling back..");
            Class<?> clazz = socket.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                socketFallback = (BluetoothSocket) m.invoke(socket.getRemoteDevice(), params);
                socketFallback.connect();
                socket = socketFallback;
                btInit();
            } catch (Exception e2) {
                Log.e("OBD", "Couldn't fallback while establishing Bluetooth connection.", e2);
            }
            e.printStackTrace();
            bollConnect = false;
            Toast.makeText(getActivity(),"Unable to connect to BT device",Toast.LENGTH_SHORT);
        }
    }

    public void btInit(){

        try {
            Log.d("OBD","ResetCommand");
            new ObdResetCommand().run(socket.getInputStream(), socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            Log.d("OBD","EchoOffCommand");
            new EchoOffCommand().run(socket.getInputStream(), socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            Log.d("OBD","LineFeedOffCommand");
            new LineFeedOffCommand().run(socket.getInputStream(), socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            Log.d("OBD","SelectProtocolCommand");
            new SelectProtocolCommand(ObdProtocols.AUTO).run(socket.getInputStream(), socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d("OBD","Running Display Commands");
        runCommands();
    }

    public void runCommands(){
        RPMCommand engineRpm = new RPMCommand();
        SpeedCommand speed = new SpeedCommand();
        Log.d("OBD","Going in the Loop");
        while (!Thread.currentThread().isInterrupted())
        {
            try {
                engineRpm.run(socket.getInputStream(), socket.getOutputStream());
                tvObdEngineRpm.setText(engineRpm.getFormattedResult());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                speed.run(socket.getInputStream(), socket.getOutputStream());
                tvObdEngineSpeed.setText(speed.getFormattedResult());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // TODO handle commands result
           Log.d("OBD", "RPM: " + engineRpm.getFormattedResult());
           Log.d("OBD", "Speed: " + speed.getFormattedResult());
        }
    }

    // Receivers

    private final BroadcastReceiver btStatsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        tvBtAdapterStat.setText("OFF");
                        tvBtAdapterStat.setTextColor(Color.parseColor("#ff0000"));
                        btk_getBtList.setEnabled(false);
                        break;
                    case BluetoothAdapter.STATE_ON:
                        tvBtAdapterStat.setText("ON");
                        tvBtAdapterStat.setTextColor(Color.parseColor("#33cc33"));
                        btk_getBtList.setEnabled(true);
                        break;
                }
            }
        }
    };
}
