package pt.antoniofreire.sensordata.MobileSensor;

import android.app.Dialog;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import pt.antoniofreire.sensordata.R;

import static android.content.Context.SENSOR_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class SensorFragment extends Fragment{

    private View v;
    private Dialog dialog;
    private SensorManager mSensorManager;
    private Sensor mLight, mLAcceleration, mRVector, mGravity;
    private TextView vallLight,
            vallAcceleration1, vallAcceleration2, vallAcceleration3,
            vallRotationVector1, vallRotationVector2, vallRotationVector3,
            vallGravity1, vallGravity2, vallGravity3;

    public SensorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Sensor","Sensor Fragment created");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_sensor, container, false);

        // New instance to sensor service
        mSensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);

        // Light Sensor
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null){
            mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        }
        else {
            mLight = null;
        }

        // Linear acceleration Sensor Listener
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null){
            mLAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        }
        else {
            mLAcceleration = null;
        }

        // Rotation Vector Sensor Listener
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null){
            mRVector = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        }
        else {
            mRVector = null;
        }

        // Gravity Sensor Listener
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null){
            mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        }
        else {
            mGravity = null;
        }

        // Get the TextViews from layout
        vallLight = (TextView) v.findViewById(R.id.vall_lightSensor);
        vallAcceleration1 = (TextView) v.findViewById(R.id.vall_linearAcceleration1);
        vallAcceleration2 = (TextView) v.findViewById(R.id.vall_linearAcceleration2);
        vallAcceleration3 = (TextView) v.findViewById(R.id.vall_linearAcceleration3);

        vallRotationVector1 = (TextView) v.findViewById(R.id.vall_rotationVector1);
        vallRotationVector2 = (TextView) v.findViewById(R.id.vall_rotationVector2);
        vallRotationVector3 = (TextView) v.findViewById(R.id.vall_rotationVector3);

        vallGravity1 = (TextView) v.findViewById(R.id.vall_gravityAcceleration1);
        vallGravity2 = (TextView) v.findViewById(R.id.vall_gravityAcceleration2);
        vallGravity3 = (TextView) v.findViewById(R.id.vall_gravityAcceleration3);


        // Dialog to show available sensor list
        Button button = (Button) v.findViewById(R.id.btk_mobile_sensor_list);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Show dialog
                dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_sensor_list);

                List<Sensor> mList= mSensorManager.getSensorList(Sensor.TYPE_ALL);

                TextView tv1 = (TextView) dialog.findViewById(R.id.textView);

                for (int i = 1; i < mList.size(); i++) {
                    tv1.append("\n" + mList.get(i).getName() + "\n" );
                }
                dialog.show();
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        // Executa quando é minimizada a aplicaç\ao e quando o fragmento é inicializado
        super.onResume();
        Log.d("Sensor","Sensor Fragment - onResume");
        // Start the service
        // getActivity().startService(new Intent(getActivity(), SensorService.class));

        // Register sensor Listener's
        if (mLight != null){
            mSensorManager.registerListener(mLightSensorListener, mLight, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mLAcceleration != null){
            mSensorManager.registerListener(mLinearAccelerationSensorListener, mLAcceleration, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mRVector != null){
            mSensorManager.registerListener(mRotationVectorSensorListener, mRVector, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mGravity != null){
            mSensorManager.registerListener(mGravitySensorListener, mGravity, SensorManager.SENSOR_DELAY_NORMAL);
        }

    }

/*    @Override
    public void onDestroyView() {
        Log.d("Sensor","Sensor Fragment view destroyed");
        super.onDestroyView();
    }*/

    @Override
    public void onPause() {
        super.onPause();
        // Unregister Listener's
        mSensorManager.unregisterListener(mLightSensorListener);
        mSensorManager.unregisterListener(mLinearAccelerationSensorListener);
        mSensorManager.unregisterListener(mRotationVectorSensorListener);
        mSensorManager.unregisterListener(mGravitySensorListener);
        // getActivity().stopService(new Intent(getActivity(), SensorService.class));
        Log.d("Sensor","Sensor Fragment - onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("Sensor","Sensor Fragment - onStop");
    }

    // ###################### Listener's ########################

    // Light Sensor Listener
    private final SensorEventListener mLightSensorListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            // The light sensor returns a single value.
            vallLight.setText(event.values[0]+" lux");
        }

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {

        }
    };

    // Linear acceleration Sensor Listener
    private final SensorEventListener mLinearAccelerationSensorListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            vallAcceleration1.setText("X="+event.values[0]);
            vallAcceleration2.setText("Y="+event.values[0]);
            vallAcceleration3.setText("Z="+event.values[0]+" m/s^2");
        }

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {

        }
    };

    // Rotation Vector Sensor Listener
    private final SensorEventListener mRotationVectorSensorListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            vallRotationVector1.setText("X="+event.values[0]);
            vallRotationVector2.setText("Y="+event.values[0]);
            vallRotationVector3.setText("Z="+event.values[0]);
        }

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {

        }
    };

    // Gravity Sensor Listener
    private final SensorEventListener mGravitySensorListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            vallGravity1.setText("X="+event.values[0]);
            vallGravity2.setText("Y="+event.values[0]);
            vallGravity3.setText("Z="+event.values[0]+" m/s^2");
        }

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {

        }
    };

}
