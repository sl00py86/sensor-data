package pt.antoniofreire.sensordata.GPSSensor;


import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.text.TextDirectionHeuristicCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import pt.antoniofreire.sensordata.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GPSFragment extends Fragment implements LocationListener {

    private View v;
    private static final String TAG = "LocationFragment";
    private LocationManager mLocationManager;
    private TextView vall_lat, vall_long, vall_alt, vall_gSpeed;


    public GPSFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d("GPS","GPS Fragment created");
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_g, container, false);

        vall_lat = (TextView) v.findViewById(R.id.vall_lat);
        vall_long = (TextView) v.findViewById(R.id.vall_long);
        vall_alt = (TextView) v.findViewById(R.id.vall_alt);
        vall_gSpeed = (TextView) v.findViewById(R.id.vall_gSpeed);

        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        Log.d("GPS","GPS Fragment View created");
        return v;
    }

  //  @Override
  //  public void onAttach(Activity activity) {
  //      super.onAttach(activity);

      //  mLocationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
  //      Log.i(TAG, "onAttach");
 //   }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(TAG, "onResume");
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.i(TAG, "onPause");
        mLocationManager.removeUpdates(this);
    }

  //  @Override
  //  public void onDetach() {
  //      super.onDetach();
  //      Log.i(TAG, "onDetach");
  //  }

    @Override
    public void onLocationChanged(Location location) {

        vall_lat.setText(""+location.getLatitude());
        vall_long.setText(""+location.getLongitude());
        vall_alt.setText(""+location.getAltitude());
        vall_gSpeed.setText(""+location.getSpeed());

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.i(TAG, "Provider " + s + " has now status: " + i);
    }

    @Override
    public void onProviderEnabled(String s) {
        Log.i(TAG, "Provider " + s + " is enabled");
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.i(TAG, "Provider " + s + " is disabled");

    }
}
